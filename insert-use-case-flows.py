import atlassian
import re

def basic_flow_field_to_dict(basic_flow_field, issue_key):
    basic_flow_dict = {}
    if basic_flow_field:
        basic_flow_regex   = r"(?umsi)(h3\. Steps).*?\n(\*|)Trigger(:|)(\*|)\s+(.*?)\n\s(.*)"
        # https://regex101.com/r/fWMMZz/1
        basic_flow_matches = re.finditer(basic_flow_regex, basic_flow_field, re.MULTILINE)
        basic_flow_match = list(basic_flow_matches)[0] # First and only match
        basic_flow_dict['orig-key'] = issue_key
        basic_flow_dict['trigger']  = basic_flow_match.groups()[4]
        basic_flow_dict['steps']    = basic_flow_match.groups()[5]
    
    return basic_flow_dict

def alt_flows_field_to_dict(alternative_flows_field, issue_key):
    alternative_flow_dict = {}
    if alternative_flows_field:
        alt_flows_regex = r"(?umsi)^h2\.\s+(.*?\n)(.*?(?:(?=(^h2\..*?\n|\Z))))"
        # https://regex101.com/r/C166st/1
        alt_flows_matches = re.finditer(alt_flows_regex, alternative_flows_field, re.MULTILINE)
        for matchNum, match in enumerate(alt_flows_matches, start=1):
            alternative_flow_dict[matchNum]             = {}
            alternative_flow_dict[matchNum]['orig-key'] = issue_key
            alternative_flow_dict[matchNum]['title']    = match.groups()[0]
            alternative_flow_dict[matchNum]['content']  = match.groups()[1]
    
    return alternative_flow_dict

def find_flows_from_use_cases(jira_instance = None, jql_query = 'project = USE and '):
    """Finds Flows embeeded in an Use Case type of issue"""
    if jql_query == '':
        return
    else:
        if not jira_instance:
            raise ValueError()
        # TODO: Get Use Cases
        issues = jira_instance.jql(jql_query)
        all_flows = {}
        for issue in issues:
            all_flows[issue['key']]={
                'parent_title':  issue['fields']['summary'],
                'basic': None,
                'alternate': {},
            }

            basic_flow_field       = issue['fields']['customfield_14508']
            if not basic_flow_field:
                print(f"{issue['key']} has no embedded Basic Flow")
            else:
                all_flows[issue['key']]['basic'] = basic_flow_field_to_dict(basic_flow_field, issue['key'])
            
            alternative_flows_field = issue['fields']['customfield_14509']
            if not alternative_flows_field:
                print(f"{issue['key']} has no embedded Alternative Flow(s)")
            else:
                # Match Alternative Flows
                all_flows[issue['key']]['alternate'] = alt_flows_field_to_dict(alternative_flows_field, issue['key'])
        return all_flows

def flow_fields_template(flow_title, flow_trigger, flow_conditions, flow_steps, parent_use_case, flow_type="Basic", flow_assemblies=['AA0.5']):
    "Builds a dictionary of fields that can be passed directly to a `jira.create_issue()` method to create a Flow"
    
    flow_template_dict = {
        'summary': flow_title,
        'project': {'key': 'USE'}, # Use Cases Project
        'issuetype': {'name': 'Flow'}, # Flow Type; could use {'id': '13200'} in our instance
        'customfield_14810': {
            # 'id': '15023',
            'value': flow_type, # If not Basic or Alternate, an error will be thrown
        }, # for a basic Flow
        'customfield_14806' :[{'key': parent_use_case}], # one entry for each parent use case; should only be 1
        'customfield_14812': flow_trigger,     # String fields
        'customfield_14507': flow_conditions,
        'customfield_14811': flow_steps,
    }
    
    # Build list of assemblies
    assembly_field_list = []
    for assembly in filter(lambda x: x in ('ITF', 'AA0.5', 'AA1', 'AA2', 'AA3', 'AA4'), flow_assemblies):
        assembly_field_list.append({'value': assembly}) # 'id' is not required if the value is valid
    flow_template_dict['customfield_12152'] = assembly_field_list
    
    
    return flow_template_dict

def create_basic_flow(jira_instance=None, basic_flow_dict=None, parent_title=""):
    """Creates a basic flow from an basic_flow_dict of this kind:
    
    {
        'orig-key': 'USE-n',
        'steps':    'h3. Pre-Conditions\nTBW\n\nh3. Steps\n# Step 1\nStep 2h3. Post-Conditions\nTBW\n\n',
        'trigger':  'The user behaviour that triggers Basic Flow in USE-n',
    },
    """
    
    basic_flow_title   = f"{parent_title} - Basic Flow"
    basic_flow_trigger = basic_flow_dict['trigger']
    basic_flow_steps   = basic_flow_dict['steps']
    basic_flow_parent  = basic_flow_dict['orig-key']
    basic_flow_fields = flow_fields_template(
        basic_flow_title,
        basic_flow_trigger,
        'Conditions TBC',
        basic_flow_steps,
        basic_flow_parent,
        "Basic",
    )
    jira_result = jira_instance.create_issue(fields =  basic_flow_fields)
    return jira_result
    
def create_alternate_flow(jira_instance=None, alternate_flow_dict=None, parent_title=""):
    """Creates an alternate flow from an alternate_flow_dict of this kind:
    
    {
        'orig-key': 'USE-n',
        'content': 'h3. Pre-Conditions\nTBW\n\nh3. Steps\n# Step 1\nStep 2h3. Post-Conditions\nTBW\n\n',
        'trigger': 'The user behaviour that triggers Alternate Flow 1 in USE-n',
    },
    """
    
    alternate_flow_title = alternate_flow_dict['title']
    alternate_flow_trigger = "Trigger TBD"
    alternate_flow_steps = alternate_flow_dict['content']
    alternate_flow_summary = f"{parent_title} - {alternate_flow_title}"
    alternate_flow_parent  = alternate_flow_dict['orig-key']
    alternate_flow_fields = flow_fields_template(
        alternate_flow_summary,
        alternate_flow_trigger,
        'Conditions TBC',
        alternate_flow_steps,
        alternate_flow_parent,
        "Alternate",
    )
    jira_result = jira_instance.create_issue(fields =  alternate_flow_fields)
    return jira_result


def create_flows_from_dict_of_flows(jira_instance=None, dict_of_flows=None):
    """Creates flows from a dictionary of flows of this kind:
    
    {
        'USE-n': {
            'parent_title': 'Title/summary of USE-n'
            'basic': {
                'orig-key': 'USE-n',
                'steps': 'h3. Steps\n# Step 1\nStep 2',
                'trigger': 'The user behaviour that triggers USE-1',
            },
            'alternate': {
                1: {
                    'orig-key': 'USE-n',
                    'content': 'h3. Pre-Conditions\nTBW\n\nh3. Steps\n# Step 1\nStep 2h3. Post-Conditions\nTBW\n\n',
                    'trigger': 'The user behaviour that triggers Alternate Flow 1 in USE-1',
                },
            
                .
                .
                .
            
                k: {
                    'orig-key': 'USE-n',
                    'content': 'h3. Pre-Conditions\nTBW\n\nh3. Steps\n# Step 1\nStep 2h3. Post-Conditions\nTBW\n\n',
                    'trigger': 'The user behaviour that triggers Alternate Flow m in USE-1',
                }
            }
        
        },
    }"""
    
    for use_case_key in dict_of_flows.keys():
        # Deal with the Basic Flow
        basic_flow = dict_of_flows[use_case_key]['basic']
        create_basic_flow(jira_instance, basic_flow, parent_title=dict_of_flows[use_case_key]['parent_title'])
        # Deal with all Alternate Flows
        for key in dict_of_flows[use_case_key]['alternate'].keys():
            alternate_flow = dict_of_flows[use_case_key]['alternate'][key]
            create_alternate_flow(
                jira_instance=jira_instance,
                alternate_flow_dict=alternate_flow,
                parent_title=dict_of_flows[use_case_key]['parent_title']
            )

if __name__ == '__main__':
    from atlassian import Jira
    jira_instance = Jira(
        url='https://jira.skatelescope.org/',
        username=input('username: '),
        password=input('password: ')
    )
    all_flows = find_flows_from_use_cases(
        jira_instance=jira_instance,
        jql_query='project = USE and issuetype="Use Case" and cf[14702] = Solution and issueLinkType != includes',
    )
    create_flows_from_dict_of_flows(jira_instance=jira_instance, dict_of_flows=all_flows)
